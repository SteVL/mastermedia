//gulp-sass - компилятор SASS
//gulp-sourcemaps - отображение файла и номера строки в инспекторе стилей
//gulp-autoprefixer - добавление префиксов
//gulp-concat - объединение файлов
//gulp-clean-css - минификация кода
//gulp - сам gulp
//gulp-if - для определения стадии (разработка или релиз)
//browser-sync - автоперезагрузка страниц при изменении файлов

//Подключение модулей
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCss = require('gulp-clean-css');
const gulpIf = require('gulp-if');

//Шаблонизатор HTML
const pug = require('gulp-pug');
//Форматирование кода HTML
const htmlbeautify = require('gulp-html-beautify');
//Плагины для уведомления об ошибках
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');



const browserSync = require('browser-sync').create();

const config = {
    paths: {
        scss: './src/scss/**/*.scss',
        chunks: './modx-dev/chunks/**/*.html',
        templates: './modx-dev/templates/*.html',
        html: './*.html',
        pug: './src/pug/*.pug',
        pugWatch: './src/pug/**/*.pug'

    },
    output: {
        cssName: 'css/style.css',
        path: './'
    },
    isDevMode: true
};

//компиляция PUG
gulp.task('pug', getHtml)
//Форматирование HTML
gulp.task('htmlbeautify', formatHtml)
//компиляция SASS
gulp.task('css', getStyles);
//создание сервера и наблюдение за изменениями
gulp.task('watch', watch);
//задача по-умолчанию      
gulp.task('default', gulp.series('css', 'pug', 'watch'));



function getHtml() {
    return gulp.src(config.paths.pug)
        .pipe(plumber({
            errorHandler: notify.onError()
        }))
        .pipe(pug())
        .pipe(htmlbeautify())
        .pipe(gulp.dest(config.output.path))        
        .pipe(browserSync.stream());
}


function formatHtml() {
    var options = {
        indentSize: 2,
        unformatted: [
            // https://www.w3.org/TR/html5/dom.html#phrasing-content
            'abbr', 'area', 'b', 'bdi', 'bdo', 'br', 'cite',
            'code', 'data', 'datalist', 'del', 'dfn', 'em', 'embed', 'i', 'ins', 'kbd', 'keygen', 'map', 'mark', 'math', 'meter', 'noscript',
            'object', 'output', 'progress', 'q', 'ruby', 's', 'samp', 'small',
            'strong', 'sub', 'sup', 'template', 'time', 'u', 'var', 'wbr', 'text',
            'acronym', 'address', 'big', 'dt', 'ins', 'strike', 'tt'
        ]
    };
   return gulp.src(config.paths.html)
        .pipe(htmlbeautify(options))
        .pipe(gulp.dest(config.output.path))
}

function getStyles() {
    return gulp.src(config.paths.scss)
        //добавление sourcemaps только в режиме разработки
        .pipe(gulpIf(config.isDevMode, sourcemaps.init()))
        .pipe(sass())
        .pipe(concat(config.output.cssName))
        .pipe(autoprefixer())
        //минификация для релиза
        .pipe(gulpIf(!config.isDevMode, cleanCss()))
        .pipe(gulpIf(config.isDevMode, sourcemaps.write()))
        //выходная папка
        .pipe(gulp.dest(config.output.path))
        //добавление в поток отслеживания
        .pipe(browserSync.stream());
}

//создание сервера и перезагрузка сервера при изменении файлов
function watch() {
    browserSync.init({
        // proxy: "pr-master.ru",
        server: {
            baseDir: config.output.path
        }
    });
    gulp.watch(config.paths.scss, getStyles);
    gulp.watch(config.paths.pugWatch, getHtml);
    // gulp.watch(config.paths.chunks).on('change', browserSync.reload);
    // gulp.watch(config.paths.templates).on('change', browserSync.reload);
    gulp.watch(config.paths.html).on('change', browserSync.reload);
}

